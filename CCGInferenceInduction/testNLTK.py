
from nltk.ccg.chart import CCGChartParser, ApplicationRuleSet, CompositionRuleSet
from nltk.ccg.chart import SubstitutionRuleSet, TypeRaiseRuleSet, printCCGDerivation
from nltk.ccg import lexicon as nkltkLex
from nltk.ccg import chart

import signal

from inOut import readTIGER as read
from preprocess import planarize as plan
from preprocess import insert as ins
from translate import constituentTypes as typ
from translate import binarize as bi
from translate import ccgCategories as ccg
from re import sub

def makeLexicon(tree):

    lexicon = []
    firstEntry = []
    firstEntry.append(tree.ccgTag)
    appendLexicon(tree, lexicon, firstEntry)

    first = ":- " + ",".join(firstEntry)
    lexicon.insert(0, first)
    lex = "\n".join(lexicon)

    lex = sub("\[\w*\]", "", lex)
    lex = sub("\$\.", "szP", lex)
    lex = sub("\$,", "szK", lex)
    lex = sub("\$\(", "szA", lex)

    return lex

def appendLexicon(node, lexicon, firstEntry):

    for child in node.children:
        if child.tag not in firstEntry:
            firstEntry.append(child.tag)
        if "/" in child.ccgTag:
            tags = child.ccgTag.split("/")
            for tag in tags:
                if "\\" in tag:
                    splitTags = tag.split("\\")
                    for t in splitTags:
                        if t not in firstEntry:
                            firstEntry.append(t)
                else:
                    if tag not in firstEntry:
                        firstEntry.append(tag)
        elif "\\" in child.ccgTag:
            tags = child.ccgTag.split("\\")
            for tag in tags:
                if tag not in firstEntry:
                    firstEntry.append(tag)

        if child.isLeaf:
            entry = child.token + " => " + child.ccgTag
            lexicon.append(entry)
        else:
            appendLexicon(child, lexicon, firstEntry)


if __name__ == "__main__":
    trees = read.extractTreefromFile("tiger_corpus_v2.xml")

    count = len(trees)
    true = 0
    hanger = []

    for testTree in trees:
        if testTree.nodeID == "s39527_507":
            continue
        print("TIGER", testTree.nodeID, ":\n-", testTree)
        plan.planarize(testTree, testTree)
        #print("planarized:\n-", testTree)
        ins.insertSBAR(testTree)
        ins.insertNP(testTree)
        ins.insertNOUN(testTree)
        bi.liftSingleNodes(testTree)
        #print("inserted:\n-", testTree)
        typ.getTypes(testTree)
        bi.liftSingleNodes(testTree)
        bi.binarize(testTree)
        print("binary:\n-", testTree)
        ccg.rootCategory(testTree)

        lexicon = makeLexicon(testTree)
        #print("\nccg-Lexicon:")
        #print(lexicon)
        parsedLex = nkltkLex.parseLexicon(lexicon)
        #parsedLex = nkltkLex.fromstring(lexicon)

        sentence = testTree.getSentence()
        #print("Sentence:\n", sentence)

        def handler(signum, frame):
            raise Exception("TIMEOUT")

        signal.signal(signal.SIGALRM, handler)
        signal.alarm(50)
        try:
            parser = CCGChartParser(parsedLex, chart.DefaultRuleSet)
            parses = parser.parse(sentence.split())
            for parse in parses:
                printCCGDerivation(parse)
                true += 1
                break
        except Exception:
            print("sentence: " + testTree.nodeID + " not parsed!")
            hanger.append(testTree.nodeID)
        signal.alarm(0)
        print("______________________")

    print("total number of sentences: ", count)
    print("parsed sentences: ", true)
    print("sentences, which cause the parser too hang up: ", len(hanger))
    for h in hanger:
        print(h)
