import xml.etree.ElementTree as eTree
from collections import OrderedDict
import preprocess.Node as Node


def extractTreefromFile(filename):
    corpus = eTree.parse(filename).getroot()
    body = corpus.find('body')
    trees = []
    for sentence in body.findall('s'):
        root = sentence.find('graph')
        disc = root.get("discontinuous")
        leafs = OrderedDict()
        nodes = OrderedDict()
        for terminals in root.findall('terminals'):
            for terminal in terminals.findall('t'):

                id = terminal.get('id')
                token = terminal.get('word')
                pos = terminal.get('pos')
               # if "$" in pos:
               #     if pos == "$.":
               #         pos = "szP"
               #     elif pos == "$,":
               #         pos = "szK"
               #     else:
               #         pos = "szA"

                secEdge = terminal.find('secedge')
                if secEdge is not None:
                    label = secEdge.get('label')
                    reference = secEdge.get('idref')
                    edge2 = (label, reference)
                else:
                    edge2 = None

                leafs[id] = (token, pos, edge2)
        for nonterminals in root.findall('nonterminals'):
            for nonterminal in nonterminals.findall('nt'):

                id = nonterminal.get('id')
                cat = nonterminal.get('cat')
                children = []
                for edge in nonterminal.findall('edge'):
                    label = edge.get('label')
                    child = edge.get('idref')
                    children.append((child, label))

                secEdge = nonterminal.find('secedge')
                if secEdge is not None:
                    label = secEdge.get('label')
                    reference = secEdge.get('idref')
                    edge2 = (label, reference)
                else:
                    edge2 = None

                nodes[id] = (cat, children, edge2)

        if len(nodes) == 0:
            children = []
            for key in leafs.keys():
                leaf = leafs[key]
                child = Node.Leaf(leaf[0], leaf[1], "", leaf[2], key, (int(key.split("_")[1]), int(key.split("_")[1])))
                children.append(child)
            children.sort()
            tree = Node.Node("S[frag]", "ROOT", None, root.get('root'), (1, children[-1].end), children)
        else:
            tree = makeTree(leafs, nodes, root.get('root'), "ROOT")
            if tree.tag == "VROOT":
                noPunct = []
                for child in tree.children:
                    if "$" not in child.tag:
                        noPunct.append(child)
                if len(noPunct) == 1:
                    if noPunct[0].isLeaf:
                        tree.tag = "S[frag]"
                    else:
                        tree = noPunct[0]
                        tree.label = "ROOT"
                elif len(noPunct) > 1:
                    tree.tag = "S[frag]"
                    #tree = Node.Node("S[frag]", "ROOT", None, root.get('root'), (1, noPunct[-1].end), noPunct)
        if tree.tag != "S[frag]":
            for key in leafs.keys():
                if "$" in leafs[key][1]:  # and leafs[key][0] != ".":
                    addPunct(tree, (key, leafs[key][0], leafs[key][1]))


        establidshEdge2(tree, tree)

        tree.disc = disc
        trees.append(tree)

    return trees


def makeTree(leafs, nodes, root, label):
    if root in nodes.keys():

        tag = nodes[root][0]
        children = []
        indices = []
        for child in nodes[root][1]:
            cLabel = child[1]
            newChild = makeTree(leafs, nodes, child[0], cLabel)
            children.append(newChild)
            indices.append(newChild.end)
            indices.sort()
        indices.sort()
        span = (indices[0], indices[-1])
        edge2 = nodes[root][2]
        return Node.Node(tag, label, edge2, root, span, children)

    else:
        token = leafs[root][0]
        tag = leafs[root][1]
        edge2 = leafs[root][2]
        index = int(root.split("_")[1])
        span = (index, index)
        return Node.Leaf(token, tag, label, edge2, root, span)


def addPunct(tree, leaf):
    added = False
    leafPos = int(leaf[0].split("_")[1])
    for child in tree.children:
        if (not child.isLeaf) and (child.start < leafPos <= child.end + 1):
            if leafPos == child.end + 1:
                child.end += 1
            addPunct(child, leaf)
            added = True
            break
    if not added:
        punct = Node.Leaf(leaf[1], leaf[2], "--", None, leaf[0], (leafPos, leafPos))
        tree.addChild(punct)


def movePunct(tree, punct):
    addPunct(tree, punct)
    punctID = punct[0]
    for child in tree.children:
        if child.nodeID == punctID:
            tree.removeChild(child)


def establidshEdge2(tree, root):
    for child in tree.children:
        if child.edge2:
            label = child.edge2[0]
            end = getNode(root, child.edge2[1])
            child.edge2 = (label, end)
        if not child.isLeaf:
            establidshEdge2(child, root)


def getNode(root, id):
    for node in root.children:
        if node.nodeID == id:
            return node
        elif not node.isLeaf:
            wanted = getNode(node, id)
            if wanted:
                return wanted
    return


def print2edges(node):
    # this is just for test purposes
    for child in node.children:
        if child.edge2:
            print(child)
            print(child.edge2[0])
            print(child.edge2[1])
        if not child.isLeaf:
            print2edges(child)


if __name__ == '__main__':
    trees = extractTreefromFile("../tiger_test")
    for tree in trees:
        print(tree)
