# -*- coding: utf8 -*-
#!/usr/bin/python

import preprocess.Node as Node


def binarize(node):
    '''
    Form the Tree to a binary tree
    Constituent types (head, complement, adjunct) have to be determined first
    :param node: root node of current subtree to be binarized
    '''
    if len(node.children) >= 3:

        tag = node.children[getHead(node.children)].tag

        if not node.children[0].constituentType == "H":

            lBranch = Node.Node(tag, "HD", None, "",
            (node.children[1].start, node.children[-1].end), node.children[1:])
            lBranch.constituentType = "H"
            for child in node.children[1:]:
                node.removeChild(child)
            node.addChild(lBranch)
            binarize(lBranch)
            if not node.children[0].isLeaf:
                binarize(node.children[0])

        else:
            rBranch = Node.Node(tag, "HD", None, "",
            (node.children[0].start, node.children[-2].end), node.children[:-1])
            rBranch.constituentType = "H"
            for child in node.children[:-1]:
                node.removeChild(child)
            node.addChild(rBranch)
            binarize(rBranch)
            if not node.children[-1].isLeaf:
                binarize(node.children[-1])

    else:
        for child in node.children:
            if not child.isLeaf:
                binarize(child)

def getHead(nodes):
    '''
    Get the head of the local tree
    :param nodes:
    :return:
    '''
    for i in range(len(nodes)):
        if nodes[i].constituentType == "H":
            return i
    return

def liftSingleNodes(node):
    for child in node.children:
        if not child.isLeaf:
            liftSingleNodes(child)
    for child in node.children:
        if len(child.children) == 1:
            child.children[0].constituentType = child.constituentType
            node.addChild(child.children[0])
            node.removeChild(child)


if __name__ == "__main__":
    import inOut.readTIGER as tiger
    import preprocess.planarize as p
    import preprocess.insert as j
    import translate.constituentTypes as c
    trees = tiger.extractTreefromFile("../tiger_extract")
    for tree in trees:
        print("TIGER: ", tree)
        p.planarize(tree)
        j.insertSBAR(tree)
        j.insertNP(tree)
        j.insertNOUN(tree)
        print("preprocessed: ", tree)
        c.getTypes(tree)
        binarize(tree)
        print("binary: ", tree)
        print("____________________")
