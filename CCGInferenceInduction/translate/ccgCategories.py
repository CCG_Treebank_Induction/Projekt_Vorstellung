# -*- coding: utf8 -*-
# !/usr/bin/python

def rootCategory(root):
    '''
    Determine the ccg category of the root node, then call getCategories() for the rest of the tree
    - Hockenmaier, 2006, p. 5
    - Hockenmaier & Steedman, 2007, p. 363
    :param root: root node of current tree
    '''
    if root.tag == "VP":
        root.ccgTag = "S\\NP"
    elif root.tag == "S":  # TODO: Featuretypen: yes-no questions[q], fragments[frg])
        if headLabel(root, "VBZ"):
            root.ccgTag = "S[dcl]"
        elif headLabel(root.children[0], "MO") and hasLeaf(root, "PW", "MO"):
            root.ccgTag = "S[wq]"
        else:
            root.ccgTag = "S"
    elif root.tag == "VROOT":
        root.ccgTag = "S[frag]"
    elif "S" not in root.tag:
        root.ccgTag = root.tag
    else:
        root.ccgTag = "S"

    #print("Assingning ccg-Categories:")
    #print(root.tag, root.ccgTag)

    if len(root.children) == 1:
        root.children[0].ccgTag = root.ccgTag
        if not root.children[0].isLeaf:
            getCategories(root.children[0])
    else:
        getCategories(root)


def getCategories(node):
    '''
    Determine ccg category of current node
     - Hockenmaier & Steedman, 2007, p. 363
    :param node: current node of the tree
    '''
    if len(node.children) < 2:
        print(node)
    #print(node.ccgTag)
    if node.children[0].constituentType == "H":
        #print(node.tag, "-head: ", node.children[0].tag, 0)
        if node.children[1].constituentType == "C":
            #print(node.tag, "-compelement: ", node.children[1].tag, 1)

            node.children[1].ccgTag = getComplement(node.children[1])
            node.children[0].ccgTag = node.ccgTag + "/" + node.children[1].ccgTag

        elif node.children[1].constituentType == "A":
            #print(node.tag, "-adjunct: ", node.children[1].tag, 1)

            node.children[0].ccgTag = node.ccgTag
            node.children[1].ccgTag = node.ccgTag + "\\" + node.children[0].ccgTag

        elif node.children[1].constituentType == "H":
            if "FIN" in node.children[0].tag:
                node.children[1].ccgTag = getComplement(node.children[1])
                node.children[0].ccgTag = node.ccgTag + "/" + node.children[1].ccgTag
            elif "FIN" in node.children[1].tag:
                node.children[0].ccgTag = getComplement(node.children[0])
                node.children[1].ccgTag = node.ccgTag + "/" + node.children[0].ccgTag
            elif "VM" in node.children[0].tag or "VA" in node.children[0].tag:
                node.children[1].ccgTag = getComplement(node.children[1])
                node.children[0].ccgTag = node.ccgTag + "/" + node.children[1].ccgTag
            elif "VM" in node.children[1].tag or "VA" in node.children[1].tag:
                node.children[0].ccgTag = getComplement(node.children[0])
                node.children[1].ccgTag = node.ccgTag + "/" + node.children[0].ccgTag
            elif node.children[1].tag == node.children[1].tag:
                node.children[0].ccgTag = getComplement(node.children[0])
                node.children[1].ccgTag = node.ccgTag + "/" + node.children[0].ccgTag

    else:
        #print(node.tag, "-head: ", node.children[1].tag, 1)

        if node.children[0].constituentType == "C":
            #print(node.tag, "-compelement: ", node.children[0].tag, 1)

            node.children[0].ccgTag = getComplement(node.children[0])
            node.children[1].ccgTag = node.ccgTag + "\\" + node.children[0].ccgTag

        elif node.children[0].constituentType == "A":
            #print(node.tag, "-adjunct: ", node.children[0].tag, 0)

            node.children[1].ccgTag = node.ccgTag
            node.children[0].ccgTag = node.ccgTag + "/" + node.children[1].ccgTag.replace("[\w+]", "")

    if not node.children[0].isLeaf:
        getCategories(node.children[0])
    if not node.children[1].isLeaf:
        getCategories(node.children[1])


def getComplement(node):
    '''
    Determine ccg category of a node that is a complement
     - Hockenmaier, 2006, p. 5
     - Hockenmaier & Steedman, 2007, p. 363
    :param node: current node
    :return: ccg category
    '''
    if node.tag == "S":
        return "S[vlast]"
    elif node.tag in ["VP", "VZ", "CVZ", "CVP"]:
        feature = getVerbal(node)
        if not feature:
            print(node.nodeID)
        if not feature:
            print(node)
        return "S" + feature + "\\NP"
    elif node.tag == "NP" or node.tag == "N":
        return node.tag + "[]"  # TODO: Kasusbestimmung fuer NPs
    else:
        return node.tag


def getVerbal(vp):
    '''
    Get feature type for a verbal phrase
     - feature types mentioned in Hockenmaier, 2006, p. 5
     - tag descriptions from:
        http://www.ims.uni-stuttgart.de/forschung/ressourcen/korpora/TIGERCorpus/annotation/tiger_introduction.pdf
    :param vp: node that is a verbal phrase
    :return: feature Type
    '''
    if vp.tag in ["VZ", "CVZ", "VVIZU"]:
        return "[zu]"
    if vp.tag in ["VP", "CVP"]:
        if vp.children[0].tag in ["VVPP", "VAPP", "VMPP"] or vp.children[1].tag in ["VVPP", "VAPP", "VMPPP"]:
            return "[pp]"

    for child in vp.children:
        # TODO: Featuretypen: passive
        if child.isLeaf:
            if child.tag in ["VVFIN", "VVIMP", "VAFIN", "VAIMP", "VMFIN", "VMIMP"]:
                return "[v1]"  # Right feature term? Text has no description, how these should be called
            elif child.tag in ["VVINF", "VAINF", "VMINF"]:
                return "[inf]"  # Right feature term?
            elif child.tag in ["VVIZU"]:
                return "[zu]"

        elif child.tag in ["VZ", "CVZ", "VVIZU"]:
            return "[zu]"
        elif child.tag in ["VVINF", "VVAINF", "VVMINF"]:
            return "[inf]"

        if child.tag in ["VP", "CVP"]:
            return getVerbal(child)

    else:
        for child in vp.children:
            if child.tag.startswith("V"):
                return getVerbal(child)
        return "[co]"  # This is for ellipses, where the verbal phrase has no verb-like component
        # The problem is not mentioned by Hockenmeier, so I'm improvising
        # The "co" is for "coordinated, as the resulting sentences in these cases is usually coordinated


def headLabel(node, label):
    '''
    find out, if the Head of a given tree has a given edge label
    :param node: root node of current subtree
    :param label: wanted head label
    :return: True if Head has given label, False otherwise
    '''
    for child in node.children:
        if child.constituentType == "H":

            if child.label == label:
                return True

            elif not child.isLeaf:
                return headLabel(child, label)

    return False


def hasLeaf(node, pos, label):
    for child in node.children:
        if child.isLeaf and child.tag.startswith(pos):
            return True
        elif child.label == label:
            if hasLeaf(child, pos, label):
                return True
    return False


if __name__ == "__main__":
    import inOut.readTIGER as tiger
    import preprocess.planarize as p
    import preprocess.insert as i
    import translate.constituentTypes as c
    import translate.binarize as b

    # trees = tiger.extractTreefromFile("/home/yakomo/docs/uni/fsem/projekt/ressources/tiger_release_aug07.corrected.16012013.xml")
    trees = tiger.extractTreefromFile("/home/kai/Dokumente/uni/coli/sem/proj/Tiger_release_july03.xml")
    for tree in trees:
        #print("TIGER: ", tree)
        p.planarize(tree)
        i.insertSBAR(tree)
        i.insertNP(tree)
        i.insertNOUN(tree)
        #print("preprocessed: ", tree)
        c.getTypes(tree)
        b.binarize(tree)
        #print("binary: ", tree)
        rootCategory(tree)
        #print("____________________")
