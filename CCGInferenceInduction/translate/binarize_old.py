
import preprocess.Node as Node


def binarize(node):
    '''
    Form the Tree to a binary tree
    Constituent types (head, complement, adjunct) have to be determined first
    :param node: root node of current subtree to be binarized
    '''
    if len(node.children) < 3:
        print("too short: ", node)
        return

    head = getHead(node.children)
    print("current: ", node)
    print("head: ", node.children[head])

    left = [node.children[j] for j in range(head+1)]
    for l in left:
        print("l: ", l)

    if not left == []:
        if not left[0].isLeaf:
            binarize(left[0])
        lBranch = Node.Node("LeftBranch", "LB", None, "", (left[1].start, left[-1].end), left[1:])
        lBranch.constituentType = "H"
        print("lBranch: ", lBranch)
        for child in left[1:]:
            node.removeChild(child)
        node.addChild(lBranch)
        binarize(lBranch)
        print("lefted: ", node)

    right = [node.children[j] for j in range(head, len(node.children))]
    for r in right:
        print("r: ", r)

    if not right == []:
        if not right[-1].isLeaf:
            binarize(right[-1])
        rBranch = Node.Node("RightBranch", "RB", None, "", (right[0].start, right[-2].end), right[:-2])
        rBranch.constituentType = "H"
        print("rBranch: ", rBranch)
        for child in right[:-2]:
            node.removeChild(child)
        node.addChild(rBranch)
        binarize(rBranch)
        print("righted: ", node)

    #for child in node.children:
    #    if not child.isLeaf:
    #        binarize(child)


def getHead(nodes):
    '''
    Get the head of the local tree
    :param nodes:
    :return:
    '''
    for i in range(len(nodes)):
        if nodes[i].constituentType == "H":
            return i
    return


if __name__ == "__main__":
    import inOut.readTIGER as tiger
    import preprocess.planarize as p
    import preprocess.insert as i
    import translate.constituentTypes as c
    trees = tiger.extractTreefromFile("../tiger_extract")
    for tree in trees:
        print("TIGER: ", tree)
        p.planarize(tree)
        i.insertSBAR(tree)
        i.insertNP(tree)
        i.insertNOUN(tree)
        print("preprocessed: ", tree)
        c.getTypes(tree)
        binarize(tree)
        print("binary: ", tree)
        print("____________________")
