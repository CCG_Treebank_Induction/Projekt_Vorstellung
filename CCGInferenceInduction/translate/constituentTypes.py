# -*- coding: utf8 -*-
#!/usr/bin/python

def getTypes(node):
    '''
    Try to determine the head, complements and adjunct of each sub-tree
    basically guessing from:
        "[which node is head, complement or adjunct] is represented in the Tiger edge labels, and
        only a small number of additional head rules is required"
        Hockenmaier, 2006
    :param node: parent node of current sub-tree
    '''
    if node.label == "ROOT":
        node.constituentType = "H"

    head = False

    for child in node.children:
        if child.label == "HD":
            child.constituentType = "H"
            head = True
        elif child.label == "CC" or child.tag == "S[vlast]":
            child.constituentType = "C"
        else:
            child.constituentType = "C"#"A"

        if not child.isLeaf:
            getTypes(child)

    if not head:
        complementizer = False

        for child in node.children:
            if child.label == "CP":
                child.constituentType = "H"
                head = True
                complementizer = True
            elif complementizer:
                child.constituentType = "C"

    if not head:
        head = specialHeadRules(node)

    if not head:
        print("No Head: ", node.nodeID, node)


def getHead(node):
    '''
    Try to determine the head among the children of the given node
    :param node: parent node of current sub-tree
    '''

    head = False

    for child in node.children:
        if child.label == "HD":
            child.constituentType = "H"
            head = True

    if not head:

        for child in node.children:
            if child.label == "CP":
                child.constituentType = "H"
                head = True

    if not head:
        head = specialHeadRules(node)

    if not head:
        print("No Head: ", node.nodeID, node)

def specialHeadRules(node):

    head = False

    if node.tag == "PP":
        for child in node.children:
            if child.tag in ["APPR", "APPRART", "APPO", "APPO", "APZR", "PROAV"] or child.label == "AC" \
                    or (child.label == "MNR" and child.tag in ["PP", "CPP"]):
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if child.tag in ["PP", "CPP", "PTKVZ"]:
                    child.constituentType = "H"
                    head = True
                    break
        if not head:
            for child in node.children:
                if child.tag in ["NP", "N", "NN", "NE"]:  # In case the PP has
                                                                # - for whatever reasons - no Preposition
                                                                # -> TODO: Find out why these phrases are labeled PP
                    child.constituentType = "H"
                    head = True
                    break
    elif node.tag == "N":
        for child in node.children:
            if child.tag in ["N", "NN", "NE", "NNE", "NP", "CNP", "PN", "FM", "CARD"]:
                # The tag "NNE" doesn't seem to formally exist in the TIGER annotation;
                #  I'm guessing the occurrence in the corpus is a typo
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if child.tag == "CH":
                    child.constituentType = "H"
                    head = True
                    break
    elif node.tag == "NP":
        for child in node.children:
            if child.tag in ["ART", "PPOSAT", "PN", "PDS", "CARD", "NN", "NE", "NP"]:
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if not (child.label == "PKT" or child.label == "AG"):
                    child.constituentType = "H"
                    head = True
                    break
    elif node.tag == "VP":
        for child in node.children:
            if child.tag.startswith("V"):
                child.constituentType = "H"
                head = True
                break
        if not head:  # This is for cases, where a VP has no Verb-like child,
                        # I have no idea how else to deal with this...
            node.children[0].constituentType = "H"
            head = True
    elif node.tag == "AVP":
        for child in node.children:
            if child.tag in ["ADV", "AVP", "PROAV"]:
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if child.tag in ["KOUS", "KOKOM", "AP"]:
                    child.constituentType = "H"
                    head = True
                    break
        if not head:
            for child in node.children:
                if child.tag == "FM":
                    child.constituentType = "H"
                    head = True
                    break
    elif node.tag == "AP":
        for child in node.children:
            if child.tag.startswith("ADJ") or (child.tag == "AP" and not child.constituentType == "C"):
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if child.tag in ["ADV", "NP"]:
                    child.constituentType = "H"
                    head = True
                    break
    else:
        for child in node.children:
            if not (child.label in ["ARG", "PKT"]):
                child.constituentType = "H"
                head = True
                break
        if not head:
            for child in node.children:
                if not child.tag == "PKT":
                    child.constituentType = "H"
                    head = True
                    break
    return head


if __name__ == "__main__":
    import inOut.readTIGER as tiger
    import preprocess.planarize as p
    import preprocess.insert as i
    trees = tiger.extractTreefromFile("../tiger_extract")
    # trees = tiger.extractTreefromFile("/home/kai/Dokumente/uni/coli/sem/proj/Tiger_release_july03.xml")
    for tree in trees:
        p.planarize(tree)
        i.insertSBAR(tree)
        i.insertNP(tree)
        i.insertNOUN(tree)

            #print("IndexError: ", tree)
        getTypes(tree)
        print(tree)
