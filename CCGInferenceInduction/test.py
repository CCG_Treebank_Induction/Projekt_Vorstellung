
from inOut import readTIGER as read
from preprocess import planarize as plan
from preprocess import insert as ins
#from preprocess import coordinate as co
from translate import constituentTypes as typ
from translate import binarize as bi
from translate import ccgCategories as ccg

#trees = read.extractTreefromFile("/home/kai/Dokumente/uni/coli/sem/proj/Tiger_release_july03.xml")
trees = read.extractTreefromFile("tiger_corpus_v2.xml")
#trees = read.extractTreefromFile("tiger_test")
file = open("test_out_noSecEdges.txt", 'w')

for tree in trees:
    #print("TIGER", tree.nodeID, ":\n-", tree)
    file.write("\nTIGER " + tree.nodeID + ":\n- " + str(tree))
    plan.planarize(tree, tree)
    #print("planarized:\n-", tree)
    file.write("\nplanarized:\n- " + str(tree))
    #if co.hasEdge2(tree):
    #    file.write("\n:sec:")
        #print(tree.nodeID, ":\n-", tree)
    #    co.coordinate(tree, tree)
        #print("coordinate:\n-", tree)
    #file.write("\nplanarized:\n- " + str(tree))
    ins.insertSBAR(tree)
    ins.insertNP(tree)
    ins.insertNOUN(tree)
    bi.liftSingleNodes(tree)
    #print("inserted:\n-", tree)
    file.write("\ninserted:\n- " + str(tree))
    typ.getTypes(tree)
    bi.liftSingleNodes(tree)
    bi.binarize(tree)
    #print("binary:\n-", tree)
    file.write("\nbinary:\n- " + str(tree))
    ccg.rootCategory(tree)
    #print("______________________")
    file.write("\n______________________")

file.close()
