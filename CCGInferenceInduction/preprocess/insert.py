# -*- coding: utf8 -*-
#!/usr/bin/python
import preprocess.Node as Node


def insertNP(node):
    '''
    insert Nominal Phrases under Prepositional Phrases in the tree
    :param node: current parent node of the tree
    '''
    if node.isLeaf:
        return
    if node.tag == "PP":

        phrase = []
        i = 0

        while i < len(node.children):
            child = node.children[i]
            if not child.isLeaf:
                insertNP(child)

            # assuming the Nominal Phrase starts with a NK Edge
            #  and consists of such edges plus punctuation, sub-sentences and Noun Phrases
            if "NK" == child.label:
                while (child.label in ["NK", "PKT"] or child.tag in ["S", "N"]) and i < len(node.children):
                    phrase.append(child)
                    i += 1

                    if i < len(node.children):
                        child = node.children[i]
                    else:
                        break
                if len(phrase) >= 2:
                    newChild = Node.Node("NP", "ARG", None, None, (phrase[0].start, phrase[-1].end), phrase)
                    for word in phrase:
                        node.removeChild(word)
                    node.addChild(newChild)
                    phrase = []
                    i = -1
            phrase = []
            i += 1
    else:
        for child in node.children:
            if not child.isLeaf:
                insertNP(child)


def insertSBAR(node):

    '''
    insert SBAR Nodes, if a sub-sentence starts with a complementizer
    :param node: the current parent node of the tree
    '''
    if node.isLeaf:
        return
    if node.tag == "S" and node.children[0].label in ["CC", "CD", "CJ", "CM", "CP", "DA"]\
            and len(node.children) > 2:
        node.tag = "SBAR"
        sNode = Node.Node("S[vlast]", "ARG", None, None, (0, 0), [])
        for child in node.children[1:]:
            node.removeChild(child)
            sNode.addChild(child)
        node.addChild(sNode)

    for child in node.children:
        if not child.isLeaf:
            insertSBAR(child)


def insertNOUN(node):
    '''
    insert a Noun Phrase into all Nominal Phrases, uniting all their NK Edges
    :param node: the current parent node of the tree
    '''
    if node.isLeaf:
        return
    if not (node.tag == "N" or len(node.children) < 2):
        i = 0
        while i < len(node.children):
            child = node.children[i]
            if child.label == "NK":
                nominals = []

                while (child.label == "NK" or not hasNoun(nominals)) and i < len(node.children):
                    nominals.append(child)
                    i += 1
                    if i < len(node.children):
                        child = node.children[i]
                    else:
                        break

                if (not (node.tag == "NP" and nominals == node.children)) and (not len(nominals) < 2)\
                        and hasNoun(nominals):
                    nominal = Node.Node("N", "ARG", None, None, (nominals[0].start, nominals[-1].end), nominals)
                    for word in nominal.children:
                        node.removeChild(word)
                    node.addChild(nominal)
                    i = 0
                else:
                    i += len(node.children)
            else:
                i += 1

    for child in node.children:
        if not child.isLeaf:
            insertNOUN(child)

def hasNoun(nodes):
    for node in nodes:
        if node.tag in ["NN", "NE"]:
            return True
    return False

if __name__ == "__main__":
    import inOut.readTIGER as tiger
    import preprocess.planarize as p
    trees = tiger.extractTreefromFile("../tiger_extract")
    for tree in trees:
        print(tree)
        p.planarize(tree)
        insertSBAR(tree)
        insertNP(tree)
        insertNOUN(tree)
        print(tree)
        print("____________________")
