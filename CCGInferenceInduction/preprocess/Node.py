# -*- coding: utf8 -*-
#!/usr/bin/python

class Node:

    def __init__(self, tag, label, edge2, nodeID, span, children):
        '''
        Nonterminal Node of a parse tree
        the child nodes are sorted by their last indices
        :param tag: String constituency-phrase tag
        :param label: String dependency label of edge connecting self to parent node;
                        empty string if root
        :param span: Tupel of int index of first and last child of self
        :param children: List of Nodes with self as parent
        '''
        self.tag = tag
        self.label = label
        self.edge2 = edge2
        self.nodeID = nodeID
        self.start = span[0]
        self.end = span[1]
        self.children = []
        for child in children:
            self.addChild(child)
        self.isLeaf = False
        self.constituentType = ""
        self.ccgTag = ""
        self.disc = False

    def addChild(self, node):
        self.children.append(node)
        self.children.sort()
        self.start = self.children[0].start
        self.end = self.children[-1].end

    def removeChild(self, node):
        self.children.remove(node)
        if not self.children == []:
            self.start = self.children[0].start
            self.end = self.children[-1].end

    def hasNode(self, node):
        for child in self.children:
            if child.nodeID == node.nodeID:
                return True
            elif child.end >= node.end and child.start <= node.start and not child.isLeaf:
                found = self.hasNode(node)
                if found:
                    return True
        return False

    def getSentence(self):
        sentence = []
        for child in self.children:
            sentence.append(child.getSentence())
        return " ".join(sentence)

    def __str__(self):
        return self.label + "-" + self.tag + ":(" + " ".join([child.__str__() for child in self.children]) + ")"

    def __lt__(self, other):
        return self.end < other.end

    def __le__(self, other):
        return self.end <= other.end

    def __eq__(self, other):
        return self.end == other.end

    def __gt__(self, other):
        return self.end > other.end

    def __ge__(self, other):
        return self.end >= other.end


class Leaf(Node):

    def __init__(self, token, tag, label, edge2, nodeID, span):
        '''
        Terminal Node of parse tree
        :param token: String for the word
        :param tag: String Part-of-Speech tag
        :param label: String dependency label of edge connecting self to parent node
        :param span: Tuple of int each the index of the token (from due to inheritance)
        '''
        Node.__init__(self, tag, label, edge2, nodeID, span, children=[])
        self.token = token
        self.isLeaf = True

    def getSentence(self):
        return self.token

    def __str__(self):
        return "{" + self.label + "-" +self.tag + " " + self.token + "[" + str(self.start) + "]" + "}"
