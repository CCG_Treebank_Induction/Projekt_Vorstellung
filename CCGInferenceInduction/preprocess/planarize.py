# -*- coding: utf8 -*-
#!/usr/bin/python

from translate import constituentTypes as types

def planarize(node, tree):
    '''
    reform the tree to a planar form, so there are no crossing edges
    as the nodes are sorted by end incices,
        if the current child node starts earlier than a former child node, there have to be crossing edges
        in that case all child nodes of the current child, which are positioned before a former node are lifted
        to be children of the current parent node
    :param node: current parent node in the tree to be planarized
    :return: start position of current parent node
    '''
    starts = []

    for child in node.children:
        if child.isLeaf:
            starts.append(child.start)
        else:
            starts.append(planarize(child, tree))

        for start in starts:
            if child.start < start:
                for grandChild in child.children:
                    if grandChild.start < start:

                        types.getHead(child)
                        if grandChild.constituentType == "H":
                            child.tag = "X"
                            grandChild.constituentType = ""
                            if grandChild.label == "HD":
                                grandChild.label = "CC"

                        node.addChild(grandChild)
                        child.removeChild(grandChild)
                        #print(tree)

                if len(child.children) == 1:
                    node.addChild(child.children[0])
                    child.removeChild(child.children[0])
                if child.children == [] and child in node.children and not child.isLeaf:
                    node.removeChild(child)

    return node.start


if __name__ == "__main__":
    import inOut.readTIGER as tiger
    #trees = tiger.extractTreefromFile("../tiger_test")
    from preprocess.Node import Node
    from preprocess.Node import Leaf
    l12 = []
    l12.append(Leaf("1", "1", "1", None, 1, (1, 1)))
    l12.append(Leaf("2", "2", "2", None, 2, (2, 2)))
    l35 = []
    l35.append(Leaf("3", "3", "3", None, 3, (3, 3)))
    l35.append(Leaf("5", "5", "5", None, 5, (5, 5)))
    c12 = []
    c12.append(Node("C1", "C1", None, 101, (1, 2), l12))
    c12.append(Node("C2", "C2", None, 102, (3, 5), l35))
    l4 = Leaf("4", "4", "4", None, 4, (4, 4))
    l6910 = []
    l6910.append(Leaf("6", "6", "6", None, 6, (6, 6)))
    l6910.append(Leaf("9", "9", "9", None, 9, (9, 9)))
    l6910.append(Leaf("10", "10", "10", None, 10, (10, 10)))
    c3 = []
    c3.append(Node("C3", "C3", None, 103, (4, 4), [l4]))
    c3.append(Leaf("7", "7", "7", None, 7, (7, 7)))
    c3.append(Leaf("8", "8", "8", None, 8, (8, 8)))
    b = []
    b.append(Node("B1", "B1", None, 201, (1, 5), c12))
    b.append(Node("B2", "B2", None, 202, (4, 8), c3))
    b.append(Node("B3", "B3", None, 203, (6, 10), l6910))
    a = Node("A", "A", None, 300, (1, 10), b)
    trees = [a]
    for tree in trees:
        print(tree)
    print("----------------------")
    for tree in trees:
        planarize(tree, tree)
        print(tree)

