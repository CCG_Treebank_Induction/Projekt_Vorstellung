from preprocess import Node


def coordinate(tree, node):
    '''
    reform coordinate Phrases with secondary edges according to Hockenmaier, 2006
    :param tree: root node of tree to be reformed; only on trees with secondary edges (hasEdge2())
    :param node: root node of current subtree
    '''
    if node.tag in ["CS", "CVP"] or (node.tag in ["S", "VP"] and node.label == "ARG"):
        edges = getEdge2(tree, [])
        complementizer = None
        head = None
        for edge in edges:
            if edge[1] == "CP" and edge[2] == node:
                complementizer = edge
            if edge[1] == "HD":
                head = edge

        if complementizer:
            parent = getParent(tree, complementizer[0])
            parent.removeChild(complementizer[0])
            if node.tag == "CVP":
                tag = "VP"
            else:
                tag = "S"
            arg = Node.Node(tag, "ARG", None, None, (node.children[0].start, node.children[-1].end), [])
            for child in node.children:
                arg.addChild(child)
            node.children = []
            node.addChild(complementizer[0])
            node.addChild(arg)
            complementizer[0].edge2 = None
            if head:
                coordinate(tree, arg)

        elif head:
            parent = getParent(tree, head[0])
            parent.removeChild(head[0])
            arg = Node.Node("ARGCLUSTER", "ARG", None, None, (node.children[0].start, node.children[-1].end), [])
            for child in node.children:
                arg.addChild(child)
            node.children = []
            node.addChild(head[0])
            node.addChild(arg)
            head[0].edge2 = None

        if not(head or complementizer):
            for child in node.children:
                if not child.isLeaf and hasEdge2(child):
                    coordinate(tree, child)

    else:
        for child in node.children:
            if not child.isLeaf and hasEdge2(child):
                coordinate(tree, child)


def hasEdge2(tree):
    '''
    check if a tree has secondary edges
    :param tree: the root node of the tree to checked
    :return: True if a secondary edge is found, False otherwise
    '''
    has = False
    for child in tree.children:
        if child.edge2:
            return True
        elif not child.isLeaf:
            if hasEdge2(child):
                has = True
    return has


def getEdge2(tree, edges=[]):
    '''
    get all secondary edges of a tree
    :param tree: tree to search for edges
    :param edges: list of secondary edges; for recursion
    :return: list of all secondary edges of the given tree;
     each element in the list is a tuple, containing the "parent", the label and the "child" of the sec. edge
    '''
    for child in tree.children:
        if child.edge2:
            edges.append((child, child.edge2[0], child.edge2[1]))
        if not child.isLeaf:
            getEdge2(child, edges)
    return edges


def getParent(tree, node):
    for child in tree.children:
        if child == node:
            return tree
        elif not child.isLeaf:
            n = getParent(child, node)
            if n:
                return n